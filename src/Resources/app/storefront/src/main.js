const PluginManager = window.PluginManager

console.log('ShogunBundle: main.js')

PluginManager.register(
    'ShGallerySlider', 
    () => import('shogun/js/component/gallery-slider'), 
    '[data-sh-component="gallery-slider"]'
)